Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root "pages#home"
  devise_for :students
  
  resources :charges
  get "code-paris-apply" => "pages#apply", as: :apply
  get "code-executive-bootcamp" => "pages#applied", as: :applied
  get "code-sign-in" => "pages#sign_in", as: :devise
  get "code-subscribed" => "charges#subscribed", as: :subscribed
  
  get "lessons/chapters" => "lessons#chapters", as: :chapters
  get "lessons/objectif" => "lessons#objective", as: :objective
  get "lessons/sublime-text" => "lessons#sublime", as: :sublime
  get "lessons/premiere-page-html" => "lessons#firstpage", as: :firstpage
  get "lessons/front-end" => "lessons#front", as: :front
  get "lessons/syntaxe-html" => "lessons#syntaxhtml", as: :syntaxhtml
  get "lessons/document-html-minimal" => "lessons#minimal", as: :minimal
  get "lessons/balises-classiques" => "lessons#commontags", as: :commontags
  get "lessons/attributs" => "lessons#attributes", as: :attributes
  get "lessons/html" => "lessons#html", as: :html
  get "lessons/css" => "lessons#css", as: :css
  get "lessons/syntaxe-css" => "lessons#syntaxcss", as: :syntaxcss
  get "lessons/ou-mettre-le-css" => "lessons#putcss", as: :putcss
  get "lessons/exemple-couple-propriete-valeur" => "lessons#properties", as: :properties
  get "lessons/selecteur-class" => "lessons#selector", as: :selector
  get "lessons/colorzilla" => "lessons#break", as: :break
  get "lessons/bootstrap" => "lessons#bootstrap", as: :bootstrap
  get "lessons/barre-de-navigation" => "lessons#navbar", as: :navbar
  get "lessons/grille" => "lessons#grid", as: :grid
  get "lessons/arriere-plan" => "lessons#background", as: :background
  get "lessons/box-model" => "lessons#boxmodel", as: :boxmodel
  get "lessons/centrer" => "lessons#center", as: :center
  get "lessons/police" => "lessons#police", as: :police
  get "lessons/icones" => "lessons#icones", as: :icones
  get "lessons/git" => "lessons#git", as: :git
  get "lessons/github" => "lessons#github", as: :github
  get "lessons/heroku" => "lessons#heroku", as: :heroku
  get "lessons/customiser" => "lessons#customiser", as: :custom
  get "lessons/social" => "lessons#social", as: :social
  get "lessons/seo" => "lessons#seo", as: :seo
  get "lessons/ligne-de-commande" => "lessons#command", as: :command
  get "fastlanguages" => "pages#fast", as: :fast
  get "cpi-codepin-cours2" => "pages#codepin2", as: :codepin2

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
