# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.japprendsacoder.fr"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  add '/', priority: 0.1, changefreq: 'daily'
  add '/code-paris-apply', priority: 0.5, changefreq: 'weekly'
  add '/code-sign-in', priority: 0.4, changefreq: 'weekly'
  add '/code-executive-bootcamp', priority: 0.3, changefreq: 'monthly'

  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
