class PagesController < ApplicationController
  def home
    @submitted = true if params[:email]
  end

  def apply
  end

  def applied
    UserMailer.booking_confirmation(params[:email]).deliver
    UserMailer.booking(params[:first_name], params[:last_name], params[:age], params[:email], params[:mobile_phone], params[:computer_pc], params[:computer_mac], params[:session], params[:expectations]).deliver
  end

  def sign_in
  end

  def fast
    render layout: nil
  end

  def codepin2
    render layout: nil
  end

end