class ChargesController < ApplicationController
  
  def create

    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :card  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      customer:     customer.id,
      amount:       params[:amount],
      description:  'Formation Executive CodeCamp',
      currency:     'eur'
    )

    UserMailer.booking_confirmation(params[:stripeEmail]).deliver

    redirect_to subscribed_path

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to charges_path
  end

end