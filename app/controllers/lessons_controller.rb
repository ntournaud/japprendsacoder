class LessonsController < ApplicationController
  before_action :authenticate_student!
  layout nil

  def chapters
  	render layout: nil
  end

  def objective
  	render layout: nil
  end

  def sublime
  	render layout: nil
  end

  def firstpage
  	render layout: nil
  end

  def front
  	render layout: nil
  end

  def syntaxhtml
  	render layout: nil
  end

  def minimal
  	render layout: nil
  end

  def commontags
  	render layout: nil
  end

  def attributes
  	render layout: nil
  end

  def html
  	render layout: nil
  end

  def css
  	render layout: nil
  end

  def syntaxcss
  	render layout: nil
  end

  def putcss
  	render layout: nil
  end

  def properties
  	render layout: nil
  end

  def selector
  	render layout: nil
  end

  def break
  	render layout: nil
  end

  def bootstrap
  	render layout: nil
  end

  def navbar
  	render layout: nil
  end

  def grid
  	render layout: nil
  end

  def background
  	render layout: nil
  end

  def boxmodel
  	render layout: nil
  end

  def center
  	render layout: nil
  end

  def police
  	render layout: nil
  end

  def icones
  	render layout: nil
  end

  def git
  	render layout: nil
  end

  def github
  	render layout: nil
  end

  def heroku
  	render layout: nil
  end

  def customiser
  	render layout: nil
  end

  def social
  	render layout: nil
  end

  def seo
  	render layout: nil
  end

  def command
  	render layout: nil
  end

end
