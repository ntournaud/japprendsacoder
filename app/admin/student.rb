ActiveAdmin.register Student do
  
  index do
    column :id
    column :email
    column :created_at
    column :last_sign_in_at
    column "Nombre de connexions", :sign_in_count
    actions
  end

end
