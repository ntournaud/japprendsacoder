class UserMailer < ActionMailer::Base
  default from: "formation@japprendsacoder.fr"
 
  def booking_confirmation(email)
    mail(to: email, subject: "Votre demande pour l'Executive CodeCamp a bien été reçue !")
  end

  def subscribed(email)
    mail(to: email, subject: "Votre participation est confirmée pour l'Executive CodeCamp !")
  end

  def booking(first_name, last_name, age, email, mobile_phone, computer_pc, computer_mac, session, expectations)
    @first_name = first_name
    @last_name = last_name
    @age = age
    @email = email
    @mobile_phone = mobile_phone
    @pc = computer_pc
    @mac = computer_mac
    @session = session
    @expectations = expectations
    mail to: "formation@japprendsacoder.fr", subject: "Une nouvelle demande de réservation"
  end

  private
    def apply_params
      params.require(:user_mailer).permit(:first_name, :last_name, :age,
                                  :email, :mobile_phone, :computer_pc, :computer_mac, :expectations, :session)
    end

end
